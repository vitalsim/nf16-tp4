//
// Created by sim20 on 07/06/2023.
//

#ifndef TP4TEST_TP4_H
#define TP4TEST_TP4_H

typedef struct Position T_Position;
struct Position {
    int numeroLigne;
    int ordre;
    int indicePhrase; // attribut ajout� par mesure
    int numeroPhrase;
    struct Position* suivant;
};

typedef struct Noeud T_Noeud;
struct Noeud {
    char* mot;
    int nbOccurences;
    T_Position* listePositions;
    struct Noeud* filsGauche;
    struct Noeud* filsDroit;
} ;

typedef struct Index T_Index;
struct Index {
    T_Noeud* racine;
    int nbMotsDistincts;
    int nbMotsTotal;

};

typedef struct Mot T_Mot;
struct Mot {
    char* mot;
    int ligne;
};

typedef struct Phrase T_Phrase;
struct Phrase {
    T_Mot* mots;
    int nbMots;
    size_t nbMotsAlloues;
};

typedef struct TexteIndex T_TexteIndex;
struct TexteIndex {
    T_Phrase* phrases;
    int nbPhrases;
    size_t nbPhrasesAllouees;
};

T_Position *ajouterPosition(T_Position *listeP, int ligne, int ordre, int phrase, int indicePhrase);

int ajouterOccurence (T_Index* index, char* mot, int ligne, int ordre, int phrase, int indicePhrase);

int indexerFichier(T_Index *index, char *filename);

void afficherIndex(T_Index index);
void recurindex (T_Noeud* noeud, char* lettre);

T_Noeud* rechercherMot(T_Index *index, char *mot);
T_Noeud* parcoursRecherche(T_Noeud* nd, char* mot);

void afficherOccurencesMot(T_Index index, char *mot);

void obtenirTexteIndexRecursive(T_TexteIndex* texteIndex, T_Noeud* nd);
T_TexteIndex* obtenirTexteIndex(T_Index* index);
void construireTexte(T_Index index, char *filename);

// deux fonctions pour liberer la m�moire en partant des index
void parcoursLibererIndex(T_Noeud** nd);
void libererIndex(T_Index** index);
void libererTexteIndex(T_TexteIndex** tIndexPtr);

char majuscule(char lettre);




#endif //TP4TEST_TP4_H
