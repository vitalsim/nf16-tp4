#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include "tp4.h"


int ajouterOccurence (T_Index* index, char* mot, int ligne, int ordre, int phrase, int indicePhrase)
{
    // On ajoute 1 au nombre total de mots de l'index
    index->nbMotsTotal +=1;

    // On vérifie si le mot existe déjà dans l'index, si c'est le cas on ajoute une position au mot
    T_Noeud* mot_deja_existant = rechercherMot(index, mot);
    if (mot_deja_existant != NULL)
    {
        mot_deja_existant->listePositions = ajouterPosition(mot_deja_existant->listePositions, ligne, ordre, phrase, indicePhrase);
        // On ajoute 1 au nombre d'occurence du mot
        mot_deja_existant->nbOccurences +=1;
        return 1;
    }

    // On ajoute 1 au nombre de mot distincs de l'index (car si on est ici c'est que c'est un nouveau mot qu'on ajoute)
    index->nbMotsDistincts +=1;

    // On crée un nouveau noeud pour le mot
    T_Noeud* nouveauNoeud = malloc(sizeof(T_Noeud));
    nouveauNoeud->mot = mot;

    nouveauNoeud->listePositions = ajouterPosition(NULL, ligne, ordre, phrase, indicePhrase);
    nouveauNoeud->nbOccurences = 1;
    nouveauNoeud->filsDroit = NULL;
    nouveauNoeud->filsGauche = NULL;

    T_Noeud* noeud = index->racine;

    if (index == NULL)
    {
        printf("index NULL");
        return 1;
    }
    // Si l'arbre est vide on met notre nouveau noeud en tête de l'arbre
    if (index->racine == NULL){
        index->racine = nouveauNoeud;
        return 1;
    }

    // On trouve l'endroit où il faut insérer notre nouveau noeud
    while (noeud != NULL)
    {
        // Si le mot est plus grand alphabétiquement alors on va à droite
        if (strcasecmp(mot, noeud->mot) > 0)
        {
            // Si le noeud a un fils droit alors on point sur ce fils droit
            if (noeud->filsDroit != NULL) noeud = noeud->filsDroit;
                // Sinon on y insère notre nouveaud noeud
            else
            {
                noeud->filsDroit = nouveauNoeud;
                return 1;
            }
        }
        else if (strcasecmp(mot, noeud->mot) < 0)
        {
            if (noeud->filsGauche != NULL) noeud = noeud->filsGauche;
            else
            {
                noeud->filsGauche = nouveauNoeud;
                return 1;
            }
        }
        // Si c'est le meme mot
        else return 0;

    }
}

void afficherIndex(T_Index index)
{
    // Création d'un noeud pour le parcours de l'arbre
    T_Noeud* noeud = index.racine;
    char lettre = 'A'; // On initialise la lettre à A pour le premier affichage
    recurindex (noeud, &lettre);

}

void recurindex (T_Noeud* noeud, char* lettre)
{
    // On met la lettre en majuscule pour l'affichage, et pour que la casse des mots ne soit pas prise en compte (A et a sont la même lettre)
    *lettre = majuscule(*lettre);
    if (noeud != NULL)
    {
        recurindex(noeud->filsGauche, lettre);

        // Si la première lettre du mot est différente de la lettre affichée alors on change la lettre et on l'affiche
        if (majuscule(noeud->mot[0]) != majuscule(*lettre))
        {
            *lettre = majuscule(noeud->mot[0]);
            printf("%c\n", *lettre);
        }
        // Affichage du mot
        printf("|-- %s : %d, \n", noeud->mot, noeud->nbOccurences);
        // Affichage des positions du mot
        for (T_Position* p = noeud->listePositions; p!= NULL; p = p->suivant)
        {
            printf("|---- (l:%d, o:%d, p:%d) \n", p->numeroLigne, p->ordre, p->numeroPhrase);
        }


        recurindex(noeud->filsDroit, lettre);

    }

}


T_Position *ajouterPosition(T_Position *listeP, int ligne, int ordre, int phrase, int indicePhrase) {
    T_Position* newPos = malloc(sizeof(T_Position));
    newPos->ordre=ordre;
    newPos->numeroLigne=ligne;
    newPos->numeroPhrase=phrase;
    newPos->indicePhrase=indicePhrase;
    newPos->suivant = NULL;


    if (listeP == NULL) {
        return newPos;
    }


    if (ligne <= listeP->numeroLigne && (ligne != listeP->numeroLigne || ordre <= listeP->ordre)) {
        newPos->suivant = listeP;
        return newPos;
    }

    T_Position* p;
    for (p = listeP;
         p->suivant != NULL && ligne >= p->suivant->numeroLigne
         && (ligne != p->suivant->numeroLigne || ordre >= p->suivant->ordre); // trier par ordre quand c'est la même ligne
         p = p->suivant);

    //printf("F: %d %d %d\n", p->numeroLigne, p->ordre, p->numeroPhrase);

    newPos->suivant = p->suivant;
    p->suivant = newPos;
    return listeP;

}



int indexerFichier(T_Index *index, char *filename) {
    FILE *textfile;
    char ch;

    textfile = fopen(filename, "r");
    if(textfile == NULL)
        return 1;

    const int wChunkSize = 4;
    char *mot=malloc(wChunkSize*sizeof(char)); // word chunk
    int motIndex=0; // current index in the word

    int chIndex=1, // char index
        phIndex=1, // index de la phrase, premier à 1
        lnIndex=1, // index de la ligne
        ordre=0,
        indicePh=0,
        nbMots=0; // nombre de mots lus

    int reading = 1;
    while(reading) {
        ch = fgetc(textfile);
        if(ch == EOF) reading = 0;

        if (ch==' ' || ch=='.' || ch=='\n' || !reading) { // le '.' ou le saut de ligne permet aussi de terminer un mot. Ou il faut terminer le mot si c'est la fin du texte
            // il faut aussi vérifier que il y a pas un enchainement de point/espace car cela ne signifie pas un nouveau mot à chaque fois
            if (motIndex != 0) { // donc dans le cas où c'est 0, on ne considère pas cela comme un nouveau mot/phrase
                ++ordre; // augmenter l'ordre actuel du mot dans la phrase
                ++indicePh;
                mot[motIndex]='\0'; // important pour terminer le mot
                ajouterOccurence(index, mot, lnIndex, ordre, phIndex, indicePh);

                // on ne free pas le mot ici car il doit rester dans le noeud
                mot = malloc(wChunkSize*sizeof(char));
                motIndex = 0; // reinitialiser l'index du mot

                if (ch == '.') {
                    ++phIndex;
                    indicePh=0;
                }
            }
        }
        else {
            // completer le mot jusqu'à l'espace/point
            mot[motIndex] = ch;
            ++motIndex;
            if (motIndex % wChunkSize == 0) { // realloc si nécessaire
                mot = realloc(mot, (motIndex + wChunkSize)*sizeof(char));
            }
        }

        if (ch == '\n') {
            ++lnIndex;
            nbMots+=ordre; // manière plus simple d'augmenter le nombre de mots dans le texte
            ordre=0; // on reinitialise l'ordre du mot dans la phrase
        }

        ++chIndex;
    }

    free(mot); // le dernier mot n'a pas été complété et doit être libéré (n'est dans aucun noeud)
    fclose(textfile);
    return nbMots;
}

T_Noeud* parcoursRecherche(T_Noeud* nd, char* mot) {
    if (nd == NULL) {
        return NULL;
    }
    else if (strcasecmp(nd->mot, mot) == 0) { // si c'est le bon mot alors on retourne ça
        return nd;
    }
    else {
        T_Noeud* noeudGauche = parcoursRecherche(nd->filsGauche, mot);
        T_Noeud* noeudDroit = parcoursRecherche(nd->filsDroit, mot);

        // retourner le noeud qui a été trouvé à gauche ou à droite (dépendant d'où il est)
        if (noeudGauche != NULL)
            return noeudGauche;
        else return noeudDroit;
    }
}

T_Noeud* rechercherMot(T_Index* index, char *mot) {
    T_Noeud* ret = parcoursRecherche(index->racine, mot);
    return ret;
}

void afficherOccurencesMot(T_Index index, char *mot){
    // Construction d'un tableau contenant toutes les phrases du texte
    T_TexteIndex* texte = obtenirTexteIndex(&index);

    // On retrouve le noeud du mot dont on cherche à afficher les phrases
    T_Noeud* noeud = rechercherMot(&index, mot);
    if (noeud == NULL) {
        printf("Le mot %s n'existe pas dans l'index\n", mot);
        return;
    }

    // On parcourt la liste des positions du mot pour afficher les phrases
    for (T_Position* p = noeud->listePositions; p != NULL; p = p->suivant)
    {
        printf("Ligne %d, mot %d :\n",p->numeroLigne, p->ordre );
        T_Phrase* ph = &texte->phrases[p->numeroPhrase - 1];
        T_Mot* mots = ph->mots;
        for (int motIndex = 0; motIndex < ph->nbMots; ++motIndex) {
            if (motIndex > 0) printf(" ");
            printf("%s", mots[motIndex].mot);
        }
        printf(".\n");
    }

    libererTexteIndex(&texte);
}

void construireTexte(T_Index index, char *filename) {
    T_TexteIndex* t = obtenirTexteIndex(&index);
    T_TexteIndex texte = *t;

    FILE *fichier;
    fichier = fopen(filename, "w"); // ouvrir le fichier en écriture

    int ligne = 1;
    for (int i = 0; i < texte.nbPhrases; ++i) {
        for (int j = 0; j < texte.phrases[i].nbMots; ++j) {
            T_Phrase ph = texte.phrases[i];
            T_Mot mot = ph.mots[j];

            if (mot.ligne > ligne){
                // ajouter le nombre de sauts de lignes nécessaires lorsque nécessaire
                size_t nbSauts = mot.ligne - ligne;
                char newLine[nbSauts + 1];
                memset(newLine, '\n', nbSauts);
                newLine[nbSauts] = '\0';
                fprintf(fichier, "%s", newLine);
                ligne = mot.ligne;
            }

            if (j > 0)
                fprintf(fichier, " ");

            char* motStr = ph.mots[j].mot;
            fprintf(fichier, "%s", motStr); // écrire dans le fichier
        }
        fprintf(fichier, ". ");
    }

    fclose(fichier); // fermer le fichier
    libererTexteIndex(&t);
}

T_TexteIndex* obtenirTexteIndex(T_Index* index) {
    T_TexteIndex* texte = malloc(sizeof(T_TexteIndex));
    texte->nbPhrases = 0;
    texte->phrases = NULL;
    texte->nbPhrasesAllouees = 0;
    obtenirTexteIndexRecursive(texte, index->racine);
    return texte;
}

void obtenirTexteIndexRecursive(T_TexteIndex* texteIndex, T_Noeud* nd) {
    // Il est nécessaire d'avoir un pointeur sur le texte et non pas le texte directement car le realloc est susceptible de modifier l'emplacement mémoire
    const size_t taillePhraseChk = 12; // Taille du chunk du nombre de mots dans une phrase
    const size_t tailleTexteChk = 8; // taille du chunk du nombre de phrases

    if (nd == NULL) return;

    for (T_Position* p = nd->listePositions; p != NULL; p = p->suivant) {
        // On parcourt toutes les positions où se trouvent le mot et on place ce mot dans notre liste de phrases
        int phraseIndex = p->numeroPhrase - 1; // Il est nécessaire d'enlever 1 car les numeros commencent à 1 sinon
        int motIndex = p->indicePhrase - 1;

        // Agrandissement zone mémoire des phrases si nécessaire
        if (phraseIndex >= texteIndex->nbPhrasesAllouees) { // Si on a atteint la fin du tableau, il faut l'agrandir
            size_t newNbpa = phraseIndex + tailleTexteChk - phraseIndex % tailleTexteChk;
            texteIndex->phrases = realloc(texteIndex->phrases, newNbpa * sizeof(T_Phrase));
            texteIndex->nbPhrasesAllouees = newNbpa;

            // nécessaire car sinon les prochains sizeof ne fonctionnent pas
            for (int i = phraseIndex; i < phraseIndex+tailleTexteChk; ++i) {
                T_Phrase* ph = &texteIndex->phrases[i];
                ph->mots = NULL;
                ph->nbMotsAlloues = 0;
                ph->nbMots = 0;
            }
        }

        // Agrandissement zone mémoire des mots de la phrase si nécessaire
        T_Phrase* phrase = &(texteIndex->phrases[phraseIndex]);
        if (motIndex >= phrase->nbMotsAlloues) {
            size_t newNbma = motIndex + taillePhraseChk - motIndex % taillePhraseChk;
            phrase->mots = realloc(phrase->mots, newNbma * sizeof(T_Mot));
            phrase->nbMotsAlloues = newNbma;
        }

        // Régler le nombre de phrases dans le texte et de mots dans la phrase
        if (p->numeroPhrase > texteIndex->nbPhrases) texteIndex->nbPhrases = p->numeroPhrase;
        if (p->indicePhrase > phrase->nbMots) phrase->nbMots = p->indicePhrase;

        // on place donc le noeud dans le tableau des phrases
        T_Mot* mot = &phrase->mots[motIndex];
        mot->mot = malloc(strlen(nd->mot) + 1);
        strcpy(mot->mot, nd->mot);
        mot->ligne = p->numeroLigne;
    }

    // Procedure recursive permettant d'appeler chaque noeud de l'ABR
    obtenirTexteIndexRecursive(texteIndex, nd->filsGauche);
    obtenirTexteIndexRecursive(texteIndex, nd->filsDroit);
}

void parcoursLibererIndex(T_Noeud** nd) {
    T_Noeud* ndv = *nd;

    free(ndv->mot);
    // free des positions
    if (ndv->listePositions != NULL) {
        for (T_Position* p = ndv->listePositions; p->suivant != NULL; p = p->suivant) {
            free(p->suivant);
        }
        free(ndv->listePositions);
    }

    if (ndv->filsGauche != NULL) parcoursLibererIndex(&ndv->filsGauche);
    if (ndv->filsDroit != NULL) parcoursLibererIndex(&ndv->filsDroit);
    free(*nd);
}

void libererIndex(T_Index** index) {
    if (index == NULL || *index == NULL)
        return;

    parcoursLibererIndex(&(*index)->racine);
    free(*index);
}

void libererTexteIndex(T_TexteIndex** tIndexPtr) { // double pointeur nécessaire sinon on ne peut pas free tIndex directement
    T_TexteIndex* tIndex = *tIndexPtr;
    for (int i = 0; i < tIndex->nbPhrases; ++i) {
        T_Phrase* ph = &tIndex->phrases[i];
        if (ph->mots != NULL) {
            for (int motIndex = 0; motIndex < ph->nbMots; ++motIndex) {
                free(ph->mots[motIndex].mot);
            }
            free(tIndex->phrases[i].mots);
        }
    }
    free(tIndex->phrases);
    free(*tIndexPtr);
}

// Fonction qui permet de transformer une lettre minuscule en majuscule
char majuscule (char lettre)
{
    if ('a'<=lettre && lettre <= 'z' ){
        lettre -= 32;
    }
    return lettre;
}
