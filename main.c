#include <stdio.h>
#include <stdlib.h>
#include "tp4.h"


// Vider le buffer (utile quand on utlise des getchar() )
void viderBuffer() {
    int c = 0;
    while (c != '\n' && c != EOF) {
        c = getchar();
    }
}

int main()
{
    T_Index* index = NULL;
    char nom_fichier[100];

    // ============= MENU UTILISATEUR ============= */
    char choix = 'x';
    while (choix != '0') {
        printf("\n======================================");
        printf("\n1. Charger un fichier");
        printf("\n2. Caracteristiques de l'index");
        printf("\n3. Afficher index");
        printf("\n4. Rechercher un mot");
        printf("\n5. Afficher les occurrences d'un mot");
        printf("\n6. Construire le texte a partir de l'index");
        printf("\n7. Quitter");
        printf("\n======================================");
        printf("\n   Votre choix ? ");
        choix = getchar();
        viderBuffer(); // supprimer de l'entr�e standard le retour charriot et les �ventuels caract�res suppl�mentaires tap�s par l'utilisateur


        switch (choix) {
            case '1' : {
                //Charger un fichier: lire un fichier et le charger dans un index puis afficher le nombre de mots lus dans le fichier

                printf("nom du fichier: ");
                scanf("%s", nom_fichier);
                viderBuffer();

                if (index != NULL)
                    libererIndex(&index); // on free l'ancien si on en charge un nouveau

                index = malloc(sizeof(T_Index));
                index->racine = NULL;
                index->nbMotsDistincts = 0;
                index ->nbMotsTotal = 0;
                printf("Indexation du fichier...\n");
                indexerFichier(index, nom_fichier);
                printf("Nombre de mots dans le fichier: %d\n", index->nbMotsTotal);
                break;
            }
            case '2' : {
                //Caract�ristiques de l'index : Afficher les caract�ristiques de l�index
                printf("le nom du fichier est %s, il contient %d mot differents et %d mot au total \n", nom_fichier, index->nbMotsDistincts, index->nbMotsTotal);
                break;
            }

            case '3': {
                //Afficher index : Afficher les mots contenus dans l�index en respectant le format demand�.
                afficherIndex(*index);
                break;
            }

            case '4': {
                //Rechercher un mot : Afficher le n� de ligne, l'ordre dans la ligne, le n� de phrase pour chaque occurrence.
                char mot[100];
                printf("mot a rechercher: ");
                scanf("%s", mot);
                viderBuffer();

                T_Noeud* noeud = rechercherMot(index, mot);
                if (noeud != NULL) {
                    for (T_Position* p = noeud->listePositions; p!= NULL; p = p->suivant) {
                        printf("ligne : %d, ordre : %d, phrase : %d\n", p->numeroLigne, p->ordre, p->numeroPhrase);
                    }
                }
                else
                    printf("Le mot n'existe pas.\n");

                break;
            }
            case '5': {
                //Afficher les occurrences d�un mot : Recherche un mot dans l�index et affiche toutes les phrases dans lesquelles il apparait.
                char mot[100];
                printf("mot : ");
                scanf("%s", mot);
                viderBuffer();

                afficherOccurencesMot(*index, mot);
                break;
            }
            case '6': {
                //Construire le texte � partir de l�index : Enregistre le texte dans un fichier.
                construireTexte(*index, "texte.txt");
                break;
            }
            case '7': {
                libererIndex(&index);
                printf("Au revoir!\n");
                return 0;
            }

            default :
                printf("\n\nERREUR : votre choix n'est valide ! ");
        }
        printf("\n\n\n");
    }

    return 0;
}
